import AudioTrack from './AudioTrack'

import History from './History';

import { EventEmitter } from 'events';


export default class AudioProject extends EventEmitter {

    /**
     * The one and only audioContext, reprsents the audio graph which contains our project.
     * @see https://developer.mozilla.org/en-US/docs/Web/API/AudioContext
     */
    private audioContext = new AudioContext();


    protected history : History = new History(1);
    

    /**
     * The global cursor, where the player cursor is currently at (in seconds)
     */
    public cursor = 0;

    /**
     * Contains all the audio tracks of the project
     */
    private tracks : AudioTrack[] = [];

    constructor(){
        super();
        requestAnimationFrame(this.monitorCursorChanges.bind(this));
    }

    /**
     * Travels one step back in history.
     */
    public undo() : boolean
    {
        const didUndo = this.history.undo();
        if(didUndo){
            this.applyCurrentHistory();
            return true;
        }
        return false;
    }

    /**
     * Update the current project based on the current history version.
     * This method should be called whenever we undo/redo
     * @returns 
     */
    private applyCurrentHistory() : void 
    {
        //There must be at least one, non empty record in the hisotry stack
        if(this.getHistoryCount() <= 1)
            return ;
        
        this.clearProject();
        const object = JSON.parse(this.history.current());

        object.tracks.forEach((track : any )=> {
            const newTrack = this.newTrack();
            track.clips.forEach((clip : any) => {
                const newClip = newTrack.newClip(clip.path);
                newClip.setStartsAt(clip.startsAt);
                newClip.setOffset(clip.offset);
                newClip.setDuration(clip.duration);
                clip.effects.forEach((effect : any ) => {
                    const newEffect = newClip.addEffect(effect.name, effect.params);
                });
            });
        });
    }

    /**
     * Redos the undoed changes if possible.
     */
    public redo() : void 
    {

        this.history.redo();
        this.applyCurrentHistory();
    }

    /**
     * Saves the current project to history (If we are not at the latest history version,
     * then all the undoed versions will be overwritten. )
     */
    public commit(): void 
    {
        this.history.commit(this.toJson(), true);
        console.log('History Updated', this.getHistoryCount());
    }

    public clearProject()
    {
        this.tracks = [];
        this.audioContext = new AudioContext();
    }

    /**
     * Creates a new empty track in the project.
     * @returns AudioTrack
     */
    public newTrack() : AudioTrack {
        const track = new AudioTrack(this.audioContext);
        this.tracks.push(track);
        return track;
    }

    /**
     * Plays all the tracks
     */
    public async play(){
        this.emit('play');
        this.tracks.forEach(track => {
            track.play();
        })
        const processPromises = this.tracks.map(track => track.play());
        await Promise.all(processPromises);
    }

    public pause(){
        this.emit('pause');
        this.audioContext.suspend();
    }

    /**
     * Exports the project as json
     * @returns String
     */
    public toJson() : string {
        return JSON.stringify(this.toObject());
    }

    /**
     * Objectify the data that makes up this Project, so that we can convert it to json later
     * @returns Object
     */
    public toObject() : object {
        return {
            cursor : this.cursor,
            tracks : this.tracks.map(track => track.toObject()),
            history : this.history.toObject()
        }
    }

    public getTracks() : AudioTrack[] 
    {
        return this.tracks;
    }

    public getHistoryCount() : number 
    {
        return this.history.count();
    }

    public forget(nbVersionsToForget : number): boolean 
    {
        return this.history.forget(nbVersionsToForget);
    }

    public isInLastVersion() : boolean 
    {
        return this.history.isInLastVersion();
    }

    /**
     * Load the audio project from a previously exported json
     * @param json project json (Generally pulled from the database)
     * @returns AudioProject instance
     */
    public static createFromJson(json : string) : AudioProject {
        const object = JSON.parse(json);

        const audioProject = new AudioProject();
        object.tracks.forEach((track : any )=> {
            const newTrack = audioProject.newTrack();
            track.clips.forEach((clip : any) => {
                const newClip = newTrack.newClip(clip.path);
                newClip.setStartsAt(clip.startsAt);
                newClip.setOffset(clip.offset);
                newClip.setDuration(clip.duration);
                clip.effects.forEach((effect : any ) => {
                    const newEffect = newClip.addEffect(effect.name, effect.params);
                });
            });
        });
        if(object.hisotry != undefined)
        {
            if(object.history.cooldown !== undefined)
                audioProject.history.setCooldown(object.history.cooldown);

            if(object.history.cooldownState)
                audioProject.history.setCooldownState(object.history.cooldownState);

            if(object.history.currentIndex)
                audioProject.history.setCurrentIndex(object.history.currentIndex)

            if(object.history.stack)
                audioProject.history.setHistoryStack(object.history.stack);
        }
        

        audioProject.commit();
        return audioProject;
    }

    private monitorCursorChanges()
    {
        if(this.cursor !== this.audioContext.currentTime){
            this.cursor = this.audioContext.currentTime;
            this.emit('cursor:update', this.cursor);
        }
        requestAnimationFrame(this.monitorCursorChanges.bind(this));
    }

    public async render(): Promise<AudioBuffer>{
        const offlineAudioContext = new OfflineAudioContext({
            numberOfChannels : 2,
            length:10 * 44100,
            sampleRate : 44100
        });

        const processPromises = this.tracks.map(track => track.processTrack(offlineAudioContext));
        await Promise.all(processPromises);
        
    
        return offlineAudioContext.startRendering();
    }

    /**
     * This is a helper method which allows us to test the buffers by playing them
     * @param audioBuffer
     */
    public playBuffer(audioBuffer : AudioBuffer) {
        // Create an AudioContext instance
        const audioContext = new AudioContext();
      
        // Create an AudioBufferSourceNode
        const sourceNode = audioContext.createBufferSource();
      
        // Set the buffer property of the AudioBufferSourceNode
        sourceNode.buffer = audioBuffer;
      
        // Connect the AudioBufferSourceNode to the destination (e.g., speakers)
        sourceNode.connect(audioContext.destination);
      
        // Start playing the AudioBufferSourceNode
        sourceNode.start();
      }


}