export default class History {
    private cooldown : number;
    private stack : string[] = [''];
    private currentIndex = 0;
    private cooldownState = 0;

    constructor(cooldown = 1) {
        this.cooldown = cooldown;
    }

    public commit(data:string, force = false) : void {
        if (this.currentIndex === this.stack.length - 1) { //checking for regular history updates
            if ((this.cooldownState >= this.cooldown || this.cooldownState === 0) && force !== true) { //history updates after a new cooldown
                this.stack.push(data)
                this.currentIndex++
                this.cooldownState = 1
            } else if (this.cooldownState < this.cooldown && force !== true) { //history updates during cooldown
                this.current(data)
                this.cooldownState++
            } else if (force === true) { //force to record without cooldown
                this.stack.push(data)
                this.currentIndex++
                this.cooldownState = this.cooldown
            }
        } else if (this.currentIndex < this.stack.length - 1) { //checking for history updates after undo
            if (force !== true) { //history updates after undo
                this.stack.length = this.currentIndex + 1
                this.stack.push(data)
                this.currentIndex++
                this.cooldownState = 1
            } else if (force === true) { ////force to record after undo 
                this.stack.length = this.currentIndex + 1
                this.stack.push(data)
                this.currentIndex++
                this.cooldownState = this.cooldown
            }
        }
    }

    public undo() : boolean {
        if (this.currentIndex > 0) {
            this.currentIndex--
            return true;
        }
        return false;
    }

    /**
     * 
     * @param readOnly If true, the history will no be altered.
     * @returns returns the new state after undoing the laste history record
     */
    public redo() : boolean {
        if (this.currentIndex < this.stack.length - 1) {
            this.currentIndex++
        }
        return false;
    }

    public isInLastVersion() : boolean
    {
        return this.currentIndex == this.count() - 1;
    }

    /**
     * 
     * @param data The string which represents the latest version of our project JSON string
     * @returns The latest version of the project JSON string.
     */
    public current(data : string | null = null) : string {
        if (data) 
            this.stack[this.currentIndex] = data

        return this.stack[this.currentIndex]
    }


    /**
     * 
     * @returns The history stack count
     */
    public count() : number 
    {
        return this.stack.length;
    }

    /**
     * How many history version to forget. 
     */
    public forget(nbVersionsToForget = 1) : boolean 
    {
        if(this.currentIndex < nbVersionsToForget )
            return false;
        this.stack.splice(this.currentIndex - nbVersionsToForget, nbVersionsToForget)
        this.currentIndex -= nbVersionsToForget;
        return true;
    }

    /**
     * Objectify the data that makes up this history, so that we can convert it to json later
     * @returns Object
     */
    public toObject() : object {
        return {
            cooldown : this.cooldown,
            stack : this.stack,
            currentIndex : this.currentIndex,
            cooldownState : this.cooldownState,
        }
    }

    public setHistoryStack(stack : string[]) : void 
    {
        this.stack = stack;
    }

    public setCooldownState(state : number) : void 
    {
        this.cooldownState = state;
    }

    public setCurrentIndex(index : number) : void 
    {
        this.currentIndex = index;
    }

    public setCooldown(cooldown : number) : void 
    {
        this.cooldownState = cooldown;
    }
}