export default abstract class AudioEffect {

    protected name = '';
    public params : any = {};

  
    constructor(params : any = {})  {
      this.params = params;
    }

    public toJson() : string {
        return JSON.stringify(this.toObject());
    }

    public setParams(params : object) : this {
      this.params = params;
      return this;
    }

    public toObject() : object {
      return {
        name : this.name,
        params : this.params
      }
  }
}