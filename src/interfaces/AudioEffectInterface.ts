import AudioClip from "../AudioClip";

/**
 * Every effect must implement this interface
 */
export default interface AudioEffectInterface {

    /**
     * Applies an effect on an audio clip
     * @param audioClip the audio clip which we are apply the effect on
     */
    apply(audioClip : AudioClip, audioContext : AudioContext | OfflineAudioContext) : void;

    toObject() : object;
}