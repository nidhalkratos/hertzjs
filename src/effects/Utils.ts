import AudioEffectInterface from "../interfaces/AudioEffectInterface";
import DefaultEffect from "./DefaultEffect";
import FadeInEffect from "./FadeInEffect";
import FadeOutEffect from "./FadeOutEffect";


/**
 * 
 * @param name the name of the effect we want to retrieve
 * @param params The parameters of the effect (For example the duration for fade-in effect)
 * @param audioConext the global audio context
 * @returns AudioEffectInterface
 */
const resolveEffect = (name : string,params : any = {} , audioConext : AudioContext) : AudioEffectInterface => {
    let effect : AudioEffectInterface;
    switch(name){
        case 'fade-in':
            effect = new FadeInEffect(params); 

        break;

        case 'fade-out':
            effect = new FadeOutEffect(params); 

        break;

        //More effects here

        default:
            effect = new DefaultEffect(params);
        break;
    }
    return effect;
}

export {
    resolveEffect
}