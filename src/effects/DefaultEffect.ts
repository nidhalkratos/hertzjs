
import AudioClip from "../AudioClip";
import AudioEffect from "../AudioEffect";
import AudioEffectInterface from "../interfaces/AudioEffectInterface";

export default class DefaultEffect extends AudioEffect implements AudioEffectInterface  {
    protected name = 'default';
    
    apply(audioClip:AudioClip, audioContext : AudioContext | OfflineAudioContext): void {
        return ;
    }
    
}