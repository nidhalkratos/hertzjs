import AudioClip from "../AudioClip";
import AudioEffect from "../AudioEffect"
import AudioEffectInterface from '../interfaces/AudioEffectInterface'

export default class FadeInEffect extends AudioEffect implements AudioEffectInterface {
    protected name = 'fade-in';
    
    public apply(audioClip : AudioClip, audioContext : AudioContext | OfflineAudioContext): any {
        console.log('Applying Fade-in with params', this.params)
        const gainNode = audioContext.createGain();
        gainNode.gain.value = 0;

        const gainParam = gainNode.gain;
        const fadeInDuration = this.params?.duration ?? 2; // duration of the fade-in in seconds
        const fadeInStartTime = audioClip.getStartsAt(); // start time of the fade-in in seconds
        gainParam.setValueAtTime(0, fadeInStartTime);
        gainParam.linearRampToValueAtTime(1, fadeInStartTime + fadeInDuration);
        
        return gainNode;
    }
    
}