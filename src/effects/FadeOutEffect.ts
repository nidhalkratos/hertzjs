import AudioClip from "../AudioClip";
import AudioEffect from "../AudioEffect"
import AudioEffectInterface from '../interfaces/AudioEffectInterface'

export default class FadeOutEffect extends AudioEffect implements AudioEffectInterface {
    protected name = 'fade-out';
    
    public apply(audioClip  : AudioClip, audioContext : AudioContext | OfflineAudioContext): any {
        console.log('Applying Fade-out with params', this.params)
        const gainNode = audioContext.createGain();
        

        gainNode.gain.setValueAtTime(1, audioContext.currentTime);

        const clipDuration : any = audioClip.getDuration() ?? 0;

        const fadeOutStart = audioClip.getStartsAt() + clipDuration - (this.params.duration ?? 2); // start the fade out at the last 3 seconds
        const fadeOutDuration = this.params.duration; // fade out over 3 seconds
        gainNode.gain.setValueAtTime(1, fadeOutStart); // start with full volume
        gainNode.gain.linearRampToValueAtTime(0, fadeOutStart + fadeOutDuration); 
        
        return gainNode;
    }
}