import AudioEffectInterface from './interfaces/AudioEffectInterface';

import {resolveEffect} from './effects/Utils'

export default class AudioClip {
    /**
     * Path to the audio file, can be a relative url or an online url, for example https://somewebsite.com/file.mp3
     */
    private path: any = "";

    /**
     * List of effects applied to this clip
     */
    private effects : AudioEffectInterface[] = [];

    /**
     * startsAt
     * When does the clip start playing in time, the left bound of the clip in the editor
     */
    private startsAt = 0;

    /**
     * offset
     * How many seconds to skip in the clip for example if the clip is 10 seconds and offset is 3, then 
     * the clip will start playing from second 3 to second 10
     */

    private offset  = 0;

    /**
     * duration
     * For how long should the clip keep playing
     */

    private duration : number | undefined;

    protected audioConext : AudioContext;

    constructor(audioContext : AudioContext){
        this.audioConext = audioContext;
    }

    public addEffect(name : string, params : object = {}) : AudioEffectInterface{
        const effect = resolveEffect(name, params, this.audioConext);
        
        this.effects.push(effect);
        return effect;
    }

    public async setPath(path : any) {
        this.path = path;
    }

    public setStartsAt(second : number){
        this.startsAt = second;
    }

    public getStartsAt(){
        return this.startsAt;
    }

    public setOffset(second : number){
        this.offset = second;
    }

    public getOffset(){
        return this.offset;
    }

    public setDuration(second : number){
        this.duration = second;
    }


    public getDuration() : number | undefined{
        return this.duration ;
    }

    public toJson() : string {
        return JSON.stringify(this.toObject());
    }


    /**
     * Objectifys the This AudioClip instance for serialization
     * @returns 
     */
    public toObject() : object {
        return {
            path : this.path,
            startsAt : this.startsAt,
            offset : this.offset,
            duration : this.duration,
            effects : this.effects.map(effect => effect.toObject())
        };
    }

    /**
     * This method is responsible for applying all the effects to this clip.
     * It does it by creating as many middle nodes as necessairy. Every node is an effect.
     * Effect nodes are linked to each other like a linked list where every effect node is linked
     * to the one after it.
     * @param audioBufferSourceNode The source node with the audio buffer
     * @param audioContext Contains the audio context, if we are just rendering for export then it takes an offline context
     */
    protected applyEffects(audioBufferSourceNode : AudioBufferSourceNode, audioContext : AudioContext | OfflineAudioContext) : void {
        if(this.effects.length == 0){
            audioBufferSourceNode?.connect(audioContext.destination);
        }
        else {
            console.log('calling apply effects')
            //Retrieve the first effect and connect it with source
            let previousNode : any = this.effects[0].apply(this, audioContext);
            audioBufferSourceNode?.connect(previousNode);

            //Apply and Connect with the rest of effects
            this.effects.splice(1).forEach(effect => {
                // Connect previous effect with effect
                const node = effect.apply(this,audioContext)
                previousNode.connect(node);
                previousNode = node;

            })

            // Connect previousEffect with destination
            previousNode.connect(audioContext.destination);
        }
    }

    /**
     * Plays this clip from the audioContext. 
     * The clip will start playing exactly at the startsAt second.
     */
    public async play()
    {
        this.processClip(this.audioConext);
    }
    

    /**
     * This method is responsible for constructing and linking the audio graph for audio clip object
     * so that when we finally render, this clip is added to the renderer.
     * It creates a source node (Which is linked to the input audio buffer) and link this new audio node
     * to the list of effect nodes (like chain/linked list of nodes), and the final effect is linked 
     * to the output.
     * @param audioContext The audio context, if it is a normal AudioContext then it will output to the speaker
     * but if it is an OfflineAudioContext, then when we render it it will be exported as a buffer along 
     * side all the other clips
     */
    public async processClip(audioContext : AudioContext | OfflineAudioContext)
    {
        const audioBufferSourceNode = audioContext.createBufferSource();
        audioBufferSourceNode.buffer = await fetch(this.path).then(resp => resp.arrayBuffer()).then(buffer => audioContext.decodeAudioData(buffer))
        this.applyEffects(audioBufferSourceNode, audioContext);
        audioBufferSourceNode.start(this.startsAt, this.offset, this.duration)
    }
}