import AudioClip from './AudioClip'

/**
 * This class represents a single Audio Track, a track contains many audio clips
 */
export default class AudioTrack {
    /**
     * The list of clips in this track
     */
    private clips : AudioClip[] = [];

    /**
     * A reference to the one single audio context that of the project.
     */
    protected audioConext : AudioContext;

    constructor(audioContext : AudioContext){
        this.audioConext = audioContext;
    }

    /**
     * Adds a new clip to the track
     * @param path 
     * @returns AudioClip
     */
    public newClip(path : string) : AudioClip {
        const clip = new AudioClip(this.audioConext);
        clip.setPath(path);
        this.clips.push(clip);
        return clip;
    }

    /**
     * Serializes the AudioTrack
     * @returns string
     */
    public toJson() : string {
        return JSON.stringify(this.toObject());
    }

    /**
     * Convert the this audio track to a javascript object
     * @returns object
     */
    public toObject() : object {
        return {
            clips : this.clips.map(clip => clip.toObject())
        }
    }

    /**
     * Play the audio track
     */
    public async play(){
        this.clips.forEach(clip => {
            clip.play();
        })
        const processPromises = this.clips.map(clip => clip.play());
        await Promise.all(processPromises);
    }

    public async processTrack(offlineAudioContext : OfflineAudioContext)
    {
        this.clips.forEach((clip) => {
            clip.processClip(offlineAudioContext);
        })

        const processPromises = this.clips.map(clip => clip.processClip(offlineAudioContext));
        return await Promise.all(processPromises);
    }
}